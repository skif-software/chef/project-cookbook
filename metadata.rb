version          '0.6.2'

name             'project'
maintainer       'Serge A. Salamanka'
maintainer_email 'serge.salamanka@smarton.by'
license          'Apache v2.0'
description      'Automation procedures for your Information System'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://github.com/skif-software/project-cookbook'
issues_url 'https://github.com/skif-software/project-cookbook/issues'

chef_version     '>= 12.7' if respond_to?(:chef_version)

supports         'debian', '>= 7.0'
#TODO: supports  'ubuntu', '>= 14.04'

#TODO: review and edit dependencies
# until bug is solved https://github.com/chef-cookbooks/ssh_known_hosts/issues/77
depends 'ssh_known_hosts', '= 4.0.0'
depends 'ark', '= 3.1.0'
depends 'runit', '= 3.0.5'
depends 'mysql', '= 8.4.0'
depends 'fix_mysql2_chef_gem'
depends 'database', '= 6.1.1'
depends 'php', '= 4.2.0'
depends 'memcached', '= 4.1.0'
depends 'chef_nginx', '= 6.1.1'
depends 'sphinx'
depends 'acme', '= 3.0.0'
depends 'msmtp'
depends 'prometheus'
depends 'netdata'
depends 'htpasswd'
