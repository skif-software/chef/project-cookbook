# project library returns the integer id of the project
# it will also parse json from .project
# system library will calculate params for subsystems

class Project
  def initialize(project)
    @project = project
  end

  def to_int
    int_id = 0
    @project.chars.each do |c|
      int_id += c.ord
    end
    int_id = int_id % 98 + 1
    return int_id
  end
end
