default['project']['netdata']['address'] = '127.0.0.1'
default['netdata']['config'] = {
  'global' => {
               'access log' => 'none'
  },
  'web' => {
            'bind to' => "#{node['project']['netdata']['address']}"
           }
}
default['netdata']['health_alarm_notify'] = {}
# TODO: create netdata mysql user when root is password protected
default['netdata']['python.d'] = {
  'mysql' => {
    'mysql_socket' => {
      'name' => 'local',
      'user' => 'root',
      'update_every' => 1,
      'socket' => node['mysql']['service']['socket']
    }
  }
}
