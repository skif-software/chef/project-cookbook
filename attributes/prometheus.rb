### LINKS

# Analysing Prometheus Memory Usage
# https://www.robustperception.io/analysing-prometheus-memory-usage/
# Understanding Machine CPU usage
# https://www.robustperception.io/understanding-machine-cpu-usage/
# Scaling and Federating Prometheus
# https://www.robustperception.io/scaling-and-federating-prometheus/
# https://stackoverflow.com/questions/36575902/how-to-use-federation-to-collect-prometheus-metrics-from-multiple-prometheus-in
# Combining metrics https://github.com/prometheus/prometheus/issues/1692


# snmp_exporter https://github.com/prometheus/snmp_exporter
# node-exporter https://github.com/prometheus/node_exporter
# mysql exporter https://github.com/prometheus/mysqld_exporter
# nginx exporter https://github.com/knyar/nginx-lua-prometheus or https://github.com/hnlq715/nginx-vts-exporter
# memcached_exporter https://github.com/prometheus/memcached_exporter
# (export logs) grok_exporter https://github.com/fstab/grok_exporter
# ports reserved https://github.com/prometheus/prometheus/wiki/Default-port-allocations

# TODO: choose exporter for PHP-FPM
# https://github.com/peakgames/php-fpm-prometheus/network
# https://github.com/craigmj/phpfpm_exporter/network
# https://github.com/blablacar/phpfpm-prometheus-exporter/network
# https://golanglibs.com/top?q=php-fpm
# https://github.com/abrander/phpfpmtop
# https://github.com/peakgames/php-fpm-prometheus
# https://golanglibs.com/?q=phpfpm-prometheus-exporter

# another way to export NGINX stat by parsing access log
# https://www.martin-helmich.de/en/blog/monitoring-nginx.html
# https://github.com/martin-helmich/prometheus-nginxlog-exporter


### ATTRIBUTES

default['prometheus']['enabled'] = true

default['prometheus']['htpasswd'] = File.join(node['nginx']['dir'], 'htpassword')

default['prometheus']['version'] = '1.7.1'
default['prometheus']['checksum'] = '4779d5cf08c50ed368a57b102ab3895e5e830d6b355ca4bfecf718a034a164e0'
default['prometheus']['init_style'] = 'runit'
default['prometheus']['binary_url']  = "https://github.com/prometheus/prometheus/releases/download/v#{node['prometheus']['version']}/prometheus-#{node['prometheus']['version']}.linux-amd64.tar.gz"

# Prometheus config will be managed separately in the context of project cookbook
override['prometheus']['job_config_cookbook_name'] = 'project'
override['prometheus']['job_config_template_name'] = 'prometheus/prometheus.yml.erb'
override['prometheus']['flags']['web.listen-address'] = ':9090'
default['prometheus']['scrape_interval'] = '5s'
default['prometheus']['evaluation_interval'] = '10s'

#default['prometheus']['allow_external_config']                                            = true

default['prometheus']['targets'] = nil

#default['prometheus']['jobs'] = {
#  'WEB_SERVERS_OS' => {
#    'scrape_interval' => '10s',
#    'scrape_timeout' => '2s',
#    'metrics_path' => '/metrics',
#    'target' => '93.125.53.38:9100'
#  }
#}

## EXPORTERS

# TODO: think about making pushgateway exporter optional enabled only in the project's cookbook
#       in case it is used for some service procedure
#       this is the correct scenario as for system operations you push through textfiles
# some issues may be: scrape_target config

# note: we are going to use netdata for most of the system metrics
default['prometheus']['exporters_enabled'] = ['node_exporter', 'blackbox_exporter', 'pushgateway']

default['prometheus']['install_dir'] = '/usr/local'

default['prometheus']['node_exporter']['version'] = '0.14.0'
default['prometheus']['node_exporter']['checksum'] = 'd5980bf5d0dc7214741b65d3771f08e6f8311c86531ae21c6ffec1d643549b2e'
default['prometheus']['node_exporter']['download_url'] = 'https://github.com/prometheus/node_exporter/releases/download/v0.14.0/node_exporter-0.14.0.linux-amd64.tar.gz'
default['prometheus']['node_exporter']['port'] = '9100'
default['prometheus']['node_exporter']['textfile_directory'] = "/var/tmp/node_exporter"
default['prometheus']['node_exporter']['parameters'] = [
  "-web.listen-address=127.0.0.1:#{node['prometheus']['node_exporter']['port']}",
  "--collectors.enabled=diskstats,filesystem,hwmon,loadavg,meminfo,netdev,netstat,sockstat,stat,time,uname,vmstat,runit,systemd,textfile",
  "--collector.textfile.directory=#{node['prometheus']['node_exporter']['textfile_directory']}"
]

default['prometheus']['blackbox_exporter']['version'] = '0.8.1'
default['prometheus']['blackbox_exporter']['checksum'] = '322a780be00b5b6319aa24282466b564ee6cd984fdd9d640ad003b2c5469e93d'
default['prometheus']['blackbox_exporter']['download_url'] = 'https://github.com/prometheus/blackbox_exporter/releases/download/v0.8.1/blackbox_exporter-0.8.1.linux-amd64.tar.gz'
default['prometheus']['blackbox_exporter']['port'] = '9192'
default['prometheus']['blackbox_exporter']['config_file'] = File.join(node['prometheus']['install_dir'], 'etc', 'blackbox_exporter.yml')
default['prometheus']['blackbox_exporter']['parameters'] = [
  "--config.file=#{node['prometheus']['blackbox_exporter']['config_file']}",
  "--web.listen-address=127.0.0.1:#{node['prometheus']['blackbox_exporter']['port']}"
]

default['prometheus']['pushgateway']['version'] = '0.4.0'
default['prometheus']['pushgateway']['checksum'] = 'e1ce58b3f2c44816e748278434d6fc91e530da77dcc34b1246e1a0f25314831f'
default['prometheus']['pushgateway']['download_url'] = 'https://github.com/prometheus/pushgateway/releases/download/v0.4.0/pushgateway-0.4.0.linux-amd64.tar.gz'
default['prometheus']['pushgateway']['port'] = '9091'
default['prometheus']['pushgateway']['parameters'] = [
  "-web.listen-address=127.0.0.1:#{node['prometheus']['pushgateway']['port']}"
]

##### ! NETDATA IS USED FOR MONITORING MYSQL ! ###################################
#default['prometheus']['mysqld_exporter']['version'] = '0.10.0'
#default['prometheus']['mysqld_exporter']['checksum'] = '32797bc96aa00bb20e0b9165f6d3887fe9612b474061ee7de0189f5377b61859'
#default['prometheus']['mysqld_exporter']['download_url'] = 'https://github.com/prometheus/mysqld_exporter/releases/download/v0.10.0/mysqld_exporter-0.10.0.linux-amd64.tar.gz'
#default['prometheus']['mysqld_exporter']['port'] = '9104'
#default['prometheus']['mysqld_exporter']['parameters'] = [
#  "-config.my-cnf=#{File.join(node['project']['ops_home'], '.my.cnf')}",
#  "-web.listen-address=127.0.0.1:#{node['prometheus']['mysqld_exporter']['port']}"
#]
##################################################################################

# Script exporter
default['prometheus']['script_executer'] = '/usr/bin/script_executer'

# Custom metrics
default['prometheus']['metrics']['cron'] = 'web_cron_task_success'
