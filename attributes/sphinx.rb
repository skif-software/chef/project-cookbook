default['sphinx']['install_method'] = 'source'
default['sphinx']['version'] = '2.2.10'
default['sphinx']['use_mysql'] = true
default['sphinx']['source']['install_path'] = '/usr/local/sphinx'
default['sphinx']['install_path'] = node['sphinx']['source']['install_path']
default['sphinx']['binary_path'] = node['sphinx']['source']['binary_path']
default['sphinx']['index_path'] = File.join(node['sphinx']['install_path'], "var", "data")
default['sphinx']['indexer']['mem_limit'] = '40M'
# wordforms file is in the websites git repository
# override wordforms during chef-client run in sphinx recipe
default['sphinx']['wordforms'] = nil
# used in cookbooks/bkt/attributes/websites.rb
default['sphinx']['searchd_listen_host'] = '127.0.0.1'
default['sphinx']['searchd_listen_port'] = '9312'
#
default['sphinx']['searchd']['listen'] = ["#{node['sphinx']['searchd_listen_host']}:#{node['sphinx']['searchd_listen_port']}"]
default['sphinx']['searchd']['pid_file'] = '/var/run/sphinx.pid'
default['sphinx']['searchd']['log'] = File.join(node['sphinx']['install_path'], "var", "log", "sphinx.log")
default['sphinx']['searchd']['query_log'] = File.join(node['sphinx']['install_path'], "var", "log", "query.log")

default['sphinx']['source_config'] = {
    'type' => 'mysql',
    'sql_host' => node['mysql']['service']['bind_address'],
    'sql_user' => node['database']['project']['user'],
    'sql_pass' => node['database']['project']['user_password'],
    'sql_port' => node['mysql']['service']['port']
}

default['project']['sphinx']['enabled']    = true
default['project']['sphinx']['default']    = true
default['project']['sphinx']['home']       = 'sphinx'
default['project']['sphinx']['data']       = File.join(node['project']['sphinx']['home'], 'data')
default['project']['sphinx']['dicts']      = File.join(node['project']['sphinx']['home'], 'dicts')
default['project']['sphinx']['logs']       = File.join(node['project']['sphinx']['home'], 'logs')
default['project']['sphinx']['run']        = File.join(node['project']['sphinx']['home'], 'run')
default['project']['sphinx']['indexes']    = File.join(node['project']['sphinx']['home'], 'indexes')
default['project']['sphinx']['scripts']    = File.join(node['project']['sphinx']['home'], 'scripts')
default['project']['sphinx']['autoconfig'] = File.join(node['project']['sphinx']['home'], 'autoconfig')
default['project']['sphinx']['config']     = File.join(node['project']['sphinx']['home'], 'sphinx.conf')
default['project']['sphinx']['conf.d']     = File.join(node['project']['sphinx']['home'], 'conf.d')
default['project']['sphinx']['host']       = '127.0.0.1'
