node.default['nginx']['default_site_enabled'] = false

default['nginx']['fastcgi_intercept_errors']     = 'on'
default['nginx']['fastcgi_ignore_client_abort']  = 'off'
default['nginx']['fastcgi_connect_timeout']      = '300'
default['nginx']['fastcgi_send_timeout']         = '300'
default['nginx']['fastcgi_read_timeout']         = '300'
default['nginx']['fastcgi_buffer_size']          = '128k'
default['nginx']['fastcgi_buffers']              = '8 256k'
default['nginx']['fastcgi_busy_buffers_size']    = '256k'
default['nginx']['fastcgi_temp_file_write_size'] = '10m'

default['nginx']['ssl']['enabled']     = false
default['nginx']['acme']['enabled']    = false
default['nginx']['http']['redirected'] = false

default['nginx']['ssl']['dir']   = '/etc/nginx/ssl'
default['nginx']['ssl']['certs'] = '/etc/nginx/ssl/certs'
default['nginx']['ssl']['live']  = '/etc/nginx/ssl/live'

default['htpasswd']['build-in']['lang'] = 'ruby'
