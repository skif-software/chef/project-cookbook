default['php']['install_method'] = 'package'
default['php']['version'] = '7.0.17'
#################################################
# main php packages
default['php']['packages'] = %w(php7.0-cgi php7.0 php7.0-dev php7.0-cli php-pear)
# additional php packages
default['php']['packages'] << %w{php7.0-mysql php7.0-memcached}
## for core/app/models/catalogue/speller_yandex_model.php
default['php']['packages'] << 'php7.0-curl'
## for image resizing script pics/image.php
default['php']['packages'] << 'php7.0-gd'
## Необходима для работы продукта в кодировке UTF-8
default['php']['packages'] << 'php7.0-mbstring'
## Функции шифрования
default['php']['packages'] << 'php7.0-mcrypt'
## Для LDAP-авторизации
default['php']['packages'] << 'php7.0-ldap'
## Для создания архивов
default['php']['packages'] << 'php7.0-zip'
##################################################
default['php']['mysql']['package'] = 'php7.0-mysql'
default['php']['mysql']['install_mysql.so_extension'] = false
default['php']['memcached']['install_memcache.so_extension'] = false
default['php']['conf_dir'] = '/etc/php/7.0/cli'
default['php']['ext_conf_dir']  = '/etc/php/7.0/cli/conf.d'
default['php']['ini']['template'] = 'php/php.ini.erb'
default['php']['ini']['cookbook'] = 'project'
default['php']['directives'] = {
    'mysqli.default_socket' => File.join(node['mysql']['service']['socket_dir'], 'mysql', 'mysqld.sock'),
    'mbstring.func_overload' => '2',
    'mbstring.internal_encoding' => 'UTF-8',
    'short_open_tag' => 'On',
    'date.timezone' => 'Europe/Minsk',
    'opcache.revalidate_freq' => '0',
    'max_input_vars' => '20000',
}

default['php']['fpm_package']   = 'php7.0-fpm'
default['php']['fpm_pooldir']   = '/etc/php/7.0/fpm/pool.d'
default['php']['fpm_user']      = 'www-data'
default['php']['fpm_group']     = 'www-data'
default['php']['fpm_listen_user']  = 'www-data'
default['php']['fpm_listen_group'] = 'www-data'
default['php']['fpm_service']      = 'php7.0-fpm'
default['php']['fpm_default_conf'] = '/etc/php/7.0/fpm/pool.d/project.conf'

# not used in php cookbook (see comments in the end of the php recipe)
default['php']['fpm_conf_dir']  = '/etc/php/7.0/fpm'
default['php']['fpm_ini']['template'] = 'php/fpm_php.ini.erb'
default['php']['fpm_ini']['cookbook'] = 'project'
default['php']['fpm_directives'] = {
    'mysqli.default_socket' => File.join(node['mysql']['service']['socket_dir'], 'mysql', 'mysqld.sock'),
    'mbstring.func_overload' => '2',
    'mbstring.internal_encoding' => 'UTF-8',
    'short_open_tag' => 'On',
    'date.timezone' => 'Europe/Minsk',
    'opcache.revalidate_freq' => '0',
    'max_input_vars' => '20000',
}
default['php']['fpm'] = {
    'project' => {
        'php_value[memory_limit]' => '34M'
    }
}
default['php']['fpm_pool'] = {
    'max_children' => 76,
    'start_servers' => 36,
    'min_spare_servers' => 2,
    'max_spare_servers' => 58
}
