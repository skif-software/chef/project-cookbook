default['project']['acme'] = {
  'webroot'         => '/srv/acme/.well-known/acme-challenge',
  'nginx_root'      => '/srv/acme',
  'nginx_location'  => '/.well-known/acme-challenge/',
  'live'            => '/var/lib/acme/live',
  'email'           => nil,
  'server'          => 'https://acme-v01.api.letsencrypt.org/directory',
  'method'          => 'webroot',
  'install_cronjob' => false,
  'key_type'        => 'rsa',
  'rsa_key_size'    => '2048'
}

default['acmetool']['enabled']      = true
default['acmetool']['state_dir']    = '/var/lib/acme'
default['acmetool']['install_dir']  = '/usr/local'
default['acmetool']['version']      = 'v0.0.61'
default['acmetool']['download_url'] = "https://github.com/hlandau/acme/releases/download/v0.0.61/acmetool-v0.0.61-linux_amd64.tar.gz"
default['acmetool']['checksum']     = 'abc9708c3ee8dde57f2b3eb342fe84ab4fd7f7ec7d358f0c2c58f47843f3fe7f'
