default['ipfs']['version'] = 'v0.4.13'
default['ipfs']['src_url'] = 'https://dist.ipfs.io/go-ipfs/v0.4.13/go-ipfs_v0.4.13_linux-amd64.tar.gz'
default['ipfs']['src_checksum'] = 'aac410ae49cec7fb6788044a7d01a1e3f377f77650e6c191357831ce6cd06283'
default['ipfs']['base_dir'] = '/usr/local'
default['ipfs']['user'] = 'ipfs'
default['ipfs']['user_home'] = '/home/ipfs'
default['ipfs']['bootstrap'] = nil
default['ipfs']['swarm_key'] = nil
default['ipfs']['api'] = '/ip4/127.0.0.1/tcp/5001'
