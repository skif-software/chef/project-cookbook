# TODO: edit attributes/netdata.rb if initial_root_password is not empty
default['mysql']['service'] = {
  'run_user' => 'mysql',
  'initial_root_password' => '',
  'bind_address' => '127.0.0.1',
  'port' => '3300',
  'socket_dir' => '/run',
  'data_dir' => '/var/lib/database'
}
# for ssh-tunnel recipe:
#default['mysql']['remote_service'] = {
#  'bind_address' => '127.0.0.1',
#  'port' => '3316'
#}

# used in mysql_extra_settings.erb template
default['mysql']['extra_settings'] = [{
  'mysqld_safe' => {
                    'nice' => '0'
  }
}]

default['mysql']['extra_settings'] << {
  'mysqld' => {
               'sql_mode' => '""',
               'skip-external-locking' => nil,
               'default-storage-engine' => 'innodb',
               'transaction-isolation' => 'READ-COMMITTED',
               'max_allowed_packet' => '16M',
               'myisam-recover-options' => 'BACKUP',
               'expire_logs_days' => '10',
               'max_binlog_size' => '100M',
               'sync_binlog' => '0'
  }
}

# Cache parameters
default['mysql']['extra_settings'] << {
  'mysqld' => {
               'query_cache_size' => '32M',
               'table_open_cache' => '4096',
               'thread_cache_size' => '32',
               'key_buffer_size' => '16M',
               'thread_stack' => '128K',
               'join_buffer_size' => '2M',
               'sort_buffer_size' => '2M'
  }
}

# Parameters for temporary tables
default['mysql']['extra_settings'] << {
  'mysqld' => {
               'tmpdir' => '/tmp',
               'max_heap_table_size' => '32M',
               'tmp_table_size' => '32M'
  }
}

# InnoDB parameters
default['mysql']['extra_settings'] << {
  'mysqld' => {
               'innodb_file_per_table' => nil,
               'innodb_buffer_pool_size' => '32M',
               'innodb_flush_log_at_trx_commit' => '2',
               'innodb_log_file_size' => '64M',
               'innodb_flush_method' => 'O_DIRECT'
  }
}

# Database charset parameters
# this parameter doesn't allow chef mysql queries:
#               'skip-name-resolve' => nil
# https://dev.mysql.com/doc/refman/5.7/en/server-options.html#option_mysqld_skip-name-resolve
default['mysql']['extra_settings'] << {
  'mysqld' => {
               'character-set-server' => 'utf8',
               'collation-server' => 'utf8_unicode_ci',
               'init-connect' => "SET NAMES utf8 COLLATE utf8_unicode_ci",
               'skip-character-set-client-handshake' => nil
  }
}

default['mysql']['extra_settings'] << {
  'mysqldump' => {
                  'quick' => nil,
                  'quote-names' => nil,
                  'max_allowed_packet' => '16M',
                  'default-character-set' => 'utf8'
  }
}

default['mysql']['extra_settings'] << {
  'isamchk' => {
                'key_buffer_size' => '16M'
  }
}
