default['project']['project']             = ['project']
default['project']['project_home']        = '/home'
default['project']['project_server_root'] = 'www'
default['project']['project_config_dir']  = '.project'

default['project']['domain']            = node['fqdn']
default['project']['domain_www_prefix'] = false

default['project']['ssh-keys']['cookbook']       = 'project'
default['project']['ssh-keys']['deploy_public']  = 'ssh/id_rsa'
default['project']['ssh-keys']['deploy_private'] = 'ssh/id_rsa.pub'
default['project']['ssh-keys']['dev_public']     = nil # use to allow access for devs
# keep public key in project's cookbook and private key in data bag
default['project']['ssh-keys']['ssh_config']     = 'ssh/config.erb'

default['project']['git']['host']     = 'github.com'
default['project']['git']['ssh_port'] = 22
default['project']['git']['repo']     = 'salsa-dev/ipfs_chef_enabled_repo.git'
default['project']['git']['branch']   = 'master'

default['project']['ssl-certs']['cookbook']      = 'project'
default['project']['ssl-certs']['foolchain']     = nil
default['project']['ssl-certs']['key']           = nil

default['project']['system']['user']         = 'system'
default['project']['system']['password']     = 'systempassword'
default['project']['system']['htpasswd']     = File.join(node['nginx']['dir'], 'htpassword')

default['project']['ops_user'] = 'ops'
default['project']['ops_home'] = '/home/ops'
