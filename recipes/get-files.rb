#
# Cookbook:: project
# Recipe:: get-files
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# Pulls all the projects' binaries with IPFS

include_recipe 'project::ipfs'
include_recipe 'project::set-users-ipfs'

# we need to have the project's code in place first
include_recipe 'project::get-code'


home = node['project']['project_home']

node['project']['project'].each do |project|

  username    = project
  server_root = File.join(home, project, node['project']['project_server_root'])
  ipfs_data_file = File.join(server_root, '.ipfs')
  ipfs_data_protected = node['ipfs']['protected'][project] rescue []
  ipfs_data_skip = node['ipfs']['skip'][project] rescue []

  project_ipfs project do
    ipfs_data_file ipfs_data_file
    ipfs_data_protected ipfs_data_protected
    ipfs_data_skip ipfs_data_skip
    server_root server_root
    username username
    project project
    action :get
  end

end # |project|
