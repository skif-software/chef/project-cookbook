#
# Cookbook:: project
# Recipe:: letsencrypt
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

### INSTALL LETSENCRYPT INTERMEDIATE CERTIFICATES
# to enable Gitlab runner registration procedure through https (see ops recipe)
remote_file '/usr/local/share/ca-certificates/letsencrypt3.crt' do
  source 'https://letsencrypt.org/certs/lets-encrypt-x3-cross-signed.pem'
  owner 'root'
  group 'root'
  mode '0644'
  action :create
end
remote_file '/usr/local/share/ca-certificates/letsencrypt4.crt' do
  source 'https://letsencrypt.org/certs/lets-encrypt-x4-cross-signed.pem'
  owner 'root'
  group 'root'
  mode '0644'
  action :create
end
# This is a limiting procedure only for Debian platform
execute "update CA certificates" do
  command %Q[update-ca-certificates]
end
# this will not work after destroy.sh:
#  not_if { ::File.exist?('/etc/ssl/certs/letsencrypt3.pem') || ::File.exist?('/etc/ssl/certs/letsencrypt4.pem') }
# so we leave the execute to run every time.
