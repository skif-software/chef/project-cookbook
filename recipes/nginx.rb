#
# Cookbook:: project
# Recipe:: nginx
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# Installs and configures NGINX
# https://supermarket.chef.io/cookbooks/chef_nginx#readme

# INSTALL AND CONFIGURE NGINX
include_recipe 'chef_nginx::default'

#####################################################################

# CREATE COMMON CONFIG FOR BITRIX PROJECTS
template '/etc/nginx/conf.d/bitrix.inc' do
  source 'nginx/bitrix.inc.conf.erb'
  group 'root'
  owner 'root'
  action :create
end

# CREATE COMMON CONFIG FOR PRIVATE PROJECT'S FILES
template '/etc/nginx/conf.d/private.inc' do
  source 'nginx/private.inc.conf.erb'
  group 'root'
  owner 'root'
  action :create
end

# CREATE COMMON CONFIG FOR SYSTEM MONITOR AND METRICS
## user and password can be overriden in the environment attributes
include_recipe 'htpasswd::ruby'
htpasswd node['project']['system']['htpasswd'] do
  user node['project']['system']['user']
  password node['project']['system']['password']
end
template '/etc/nginx/conf.d/netdata.conf' do
  source 'nginx/netdata.conf.erb'
  group 'root'
  owner 'root'
  action :create
end
template '/etc/nginx/conf.d/system.inc' do
  source 'nginx/system.inc.conf.erb'
  group 'root'
  owner 'root'
  action :create
end

# CREATE COMMON CONFIG FOR SSL
if node['nginx']['ssl']['enabled']
  template '/etc/nginx/conf.d/ssl.conf' do
    source 'nginx/ssl.conf.erb'
    group 'root'
    owner 'root'
    action :create
  end
end

# CREATE COMMON CONFIG FOR ACME CHALLENGES
if node['nginx']['ssl']['enabled'] && node['nginx']['acme']['enabled']
  node.override['nginx']['http']['redirected'] = true
  template '/etc/nginx/conf.d/acme.inc' do
    source 'nginx/acme.inc.conf.erb'
    group 'root'
    owner 'root'
    action :create
  end
end

#####################################################################

# PLACE SSL CERTS INTO NGINX SSL DIRECTORY IF SSL ENABLED
if node['nginx']['ssl']['enabled']

  directory node['nginx']['ssl']['dir'] do
    owner  'root'
    group  'root'
    mode   0755
    action :create
  end

  directory node['nginx']['ssl']['certs'] do
    owner  'root'
    group  'root'
    mode   0755
    action :create
  end

  directory node['nginx']['ssl']['live'] do
    owner  'root'
    group  'root'
    mode   0755
    action :create
  end

  node['project']['project'].each do |project|
    domain            = node[project]['domain']
    home = node['project']['project_home']
    server_root = File.join(home, project, node['project']['project_server_root'])

    project_ssl_certs project do
      project     project
      domain      domain
      server_root server_root
      action      :setup
    end
  end # |project|

end

#####################################################################

# ENABLE NGINX CONFIGURATION FOR PROJECTS
home = node['project']['project_home']
node['project']['project'].each do |project|
  username    = project
  server_root = File.join(home, project, node['project']['project_server_root'])
  domain      = node[project]['domain']
  socket      = "/var/run/#{node['php']['fpm_service']}_#{project}.sock"
  domain_www_prefix = node[project]['domain_www_prefix'] ? 'www.' : ''
  nginx_site project do
    template "nginx/project.conf.erb"
    variables(project: project, domain: domain, domain_www_prefix: domain_www_prefix, server_root: server_root, socket: socket)
    action   :enable
    notifies :reload, 'service[nginx]', :immediately
  end
end

# Usage of "notifies :reload, 'service[nginx]', :delayed" is limitted because
# Resources that are executed during the compile phase cannot notify other resources.
# https://docs.chef.io/resource_common.html
service 'nginx' do
  action :nothing
end

#####################################################################

# GENERATE SSL CERTS WITH ACME IF ENABLED AND RELOAD NGINX
if node['nginx']['ssl']['enabled'] && node['nginx']['acme']['enabled']
  include_recipe 'project::get-ssl-certs'
end
