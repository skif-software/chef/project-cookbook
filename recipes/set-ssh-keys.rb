#
# Cookbook:: project
# Recipe:: set-ssh-keys
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# Creates special user for ssh passwordless access and Installs ssh keys

if node['project']['ssh-keys']['cookbook'] == nil
  log 'message' do
    message "Please, provide your ssh-keys by defining node['project']['ssh-keys']['cookbook']. Setting up default ssh-keys."
    level :info
  end
  node.override['project']['ssh-keys']['cookbook'] = 'project'
end

home = node['project']['project_home']

node['project']['project'].each do |project|

username = project

if node[project]['ssh-keys']['cookbook'] == nil
  fail "Please, provide your ssh-keys by defining node['#{project}']['ssh-keys']['cookbook'] attribute. Setting up default ssh-keys..."
end

directory File.join(home, project, '.ssh') do
  owner  username
  group  username
  mode   0700
  action :create
end

cookbook_file File.join(home, project, '.ssh', 'deploy_id_rsa.pub') do
  source   node[project]['ssh-keys']['deploy_public']
  cookbook node[project]['ssh-keys']['cookbook']
  owner    username
  group    username
  mode     0700
  action   :create
end

cookbook_file File.join(home, project, '.ssh', 'deploy_id_rsa') do
  source   node[project]['ssh-keys']['deploy_private']
  cookbook node[project]['ssh-keys']['cookbook']
  owner    username
  group    username
  mode     0700
  action   :create
end

if node[project]['ssh-keys']['dev_public']
  cookbook_file File.join(home, project, '.ssh', 'authorized_keys') do
    source   node[project]['ssh-keys']['dev_public']
    cookbook node[project]['ssh-keys']['cookbook']
    owner    username
    group    username
    action   :create
  end
end

end # |project|
