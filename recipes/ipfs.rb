#
# Cookbook:: project
# Recipe:: ipfs
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# Installs and configures IPFS

# https://github.com/ipfs/go-ipfs/issues/2183
# to access the daemon and its datastore from other users, I copy ~ipfs/.ipfs/api file to these users’ ~/.ipfs dirs (or you can use --api command line argument).

# https://github.com/ipfs/faq/issues/22
# IPFS represents the hash of files and objects using Multihash format and Base58 encoding.
# The letters Qm happen to correspond with the algorithm (SHA-256) and length (32 bytes) used by IPFS.

# NOTICE: the order of the resources is very important here

user 'ipfs' do
  home node['ipfs']['user_home']
  shell '/bin/bash'
  action :create
end

directory node['ipfs']['user_home'] do
  owner node['ipfs']['user']
  group node['ipfs']['user']
  mode '0750'
  action :create
end

# get the tar.gz file
# unpack into versioned directory and place link to ipfs binary into /usr/local/bin
ark 'ipfs' do
  url node['ipfs']['src_url']
  version node['ipfs']['version']
  checksum node['ipfs']['src_checksum']
  owner node['ipfs']['user']
  group node['ipfs']['user']
  has_binaries ['ipfs']
end

# fix after v0.4.12
file File.join(node['ipfs']['base_dir'], 'ipfs', 'ipfs') do
  mode '0755'
end

execute 'ipfs init' do
  user node['ipfs']['user']
  group node['ipfs']['user']
  command %Q[ipfs -c #{File.join(node['ipfs']['user_home'], '.ipfs')} init]
  action :run
  not_if { ::File.exist?(::File.join(node['ipfs']['user_home'], '.ipfs', 'config')) }
end

if node['ipfs']['bootstrap']

execute 'ipfs bootstrap rm --all' do
  user node['ipfs']['user']
  group node['ipfs']['user']
  command %Q[ipfs -c #{File.join(node['ipfs']['user_home'], '.ipfs')} bootstrap rm --all]
  action :run
end

execute 'ipfs bootstrap add' do
  user node['ipfs']['user']
  group node['ipfs']['user']
  command %Q[ipfs -c #{File.join(node['ipfs']['user_home'], '.ipfs')} bootstrap add #{node['ipfs']['bootstrap']}]
  action :run
end

end # if node['ipfs']['bootstrap']

include_recipe 'runit'

runit_service 'ipfs' do
  default_logger true
  sv_timeout 15
  owner node['ipfs']['user']
  group node['ipfs']['user']
  supervisor_owner node['ipfs']['user']
  supervisor_group node['ipfs']['user']
  options({
    :user => node['ipfs']['user'],
    :binary => "#{node['ipfs']['base_dir']}/bin/ipfs",
    :config => File.join(node['ipfs']['user_home'], '.ipfs'),
    :parameters => ['daemon']
  }.merge(params))
  action [:create, :enable]
end

file File.join(node['ipfs']['user_home'], '.ipfs', 'swarm.key') do
  content "/key/swarm/psk/1.0.0/
/bin/
#{node['ipfs']['swarm_key']}
"
  mode '0750'
  user node['ipfs']['user']
  group node['ipfs']['user']
  action :create
  notifies :restart, 'runit_service[ipfs]', :immediately
end

runit_service 'ipfs' do
  sv_timeout 15
  action :start
end


# DisableNatPortMap because we don't need NAT discovery (restarts of the daemon will be faster).
# https://github.com/ipfs/go-ipfs/blob/v0.4.10/CHANGELOG.md#048---2017-03-29
# https://github.com/ipfs/go-ipfs/issues/2964
execute 'ipfs config --json Swarm.DisableNatPortMap' do
  user node['ipfs']['user']
  group node['ipfs']['user']
  command %Q[ipfs -c #{File.join(node['ipfs']['user_home'], '.ipfs')} config --json Swarm.DisableNatPortMap true]
  retries 10
  retry_delay 2
  action :run
end

# DisableBandwidthMetrics to save memory usage as we are on private small network
execute 'ipfs config --json Swarm.DisableBandwidthMetrics' do
  user node['ipfs']['user']
  group node['ipfs']['user']
  command %Q[ipfs -c #{File.join(node['ipfs']['user_home'], '.ipfs')} config --json Swarm.DisableBandwidthMetrics true]
  retries 10
  retry_delay 2
  action :run
end

# TODO: consider using experimental feature
# https://github.com/ipfs/go-ipfs/blob/v0.4.10/CHANGELOG.md#048---2017-03-29
# ipfs config --json Experimental.ShardingEnabled true
# but there could be issue:
# https://github.com/ipfs/go-ipfs/pull/3042#issuecomment-261744402

# Fix high memory usage (fixed by https://github.com/ipfs/go-ipfs/blob/v0.4.11/CHANGELOG.md )
#cron 'IPFS restart' do
#  action :create
#  minute '0'
#  hour '*'
#  weekday '*'
#  user node['ipfs']['user']
#  home node['ipfs']['user_home']
#  command %Q[sv restart ipfs]
#end

#place .ipfs/api file to other users' .ipfs directory
#see recipes/set-users-ipfs.rb
