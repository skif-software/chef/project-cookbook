#
# Cookbook:: project
# Recipe:: mysql
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

######################################################################
# comment: setting trusted solves periodic keyserver timouts
######################################################################
#    STDERR: gpg: requesting key 5072E1F5 from hkp server pgp.mit.edu
#    gpg: keyserver timed out
#    gpg: keyserver receive failed: keyserver error
######################################################################
apt_repository 'mysql' do
  uri        'http://repo.mysql.com/apt/debian/'
  components ['mysql-5.7']
  trusted true
#  key '5072E1F5'
#  keyserver 'pgp.mit.edu'
  action :add
end

# TODO: multiple projects on one server
# Here the multiple instances of MySQL should be used to provide
# full access to mysqld server to admins of different projects,
# creating a provider-like environment for the server admin.
# https://dev.mysql.com/doc/refman/5.7/en/multiple-servers.html
# https://dev.mysql.com/doc/refman/5.7/en/multiple-unix-servers.html
# This feature can also be used for testing different versions of mysqld
# and will certainly be used for backups with Percona XtraBackup tool.
# https://www.percona.com/blog/2014/08/26/mysqld_multi-how-to-run-multiple-instances-of-mysql/
# Here are some considerations on performance
# http://itknowledgeexchange.techtarget.com/sql-server/single-instance-vs-multiple-instances/
# SQL Server will use as much memory as you can throw at it (pretty much), and it is always advisable to set a limit on every instance.
# https://dba.stackexchange.com/a/40456
# How MySQL Uses Memory https://dev.mysql.com/doc/refman/5.7/en/memory-use.html
# innodb_buffer_pool_size can be configured dynamically, while the server is running. For more information, see Section 14.6.3.2, “Configuring InnoDB Buffer Pool Size”.
# https://mariadb.com/kb/en/library/mariadb-memory-allocation/
# http://mysql.rjweb.org/doc.php/memory

# Configure the MySQL client.
# you can install different version if you like
# see https://github.com/chef-cookbooks/mysql#mysql_client
mysql_client 'default' do
  version '5.7'
  package_name ['mysql-client', 'libmysqlclient-dev']
  action :create
end

home = node['project']['project_home']

node['project']['project'].each do |project|

project_id = Project.new(project)

user "#{node['mysql']['service']['run_user']}-#{project}" do
  comment 'MySQL Server'
  system true
  shell '/bin/false'
  home File.join(node['mysql']['service']['data_dir'], "mysql-#{project}")
  action :create
end

# MySQL server is now installed and customized for every project.
mysql_service project do
  bind_address node['mysql']['service']['bind_address']
  port node['mysql']['service']['port'].to_i + project_id.to_int
  socket File.join(node['mysql']['service']['socket_dir'], "mysql-#{project}", 'mysqld.sock')
  data_dir File.join(node['mysql']['service']['data_dir'], "mysql-#{project}")
  version '5.7'
  package_name 'mysql-server'
  initial_root_password node['mysql']['service']['initial_root_password']
  run_user  "#{node['mysql']['service']['run_user']}-#{project}"
  run_group "#{node['mysql']['service']['run_user']}-#{project}"
  action [:create, :start]
end

# Bitrix reqires sql_mode to be setup to empty string.
mysql_config project do
  instance project
  source 'mysql_extra_settings.erb'
  variables({
      :options => node['mysql']['extra_settings']
    })
  owner "#{node['mysql']['service']['run_user']}-#{project}"
  group "#{node['mysql']['service']['run_user']}-#{project}"
  notifies :restart, "mysql_service[#{project}]", :immediately
  action :create
end

end # node['project']['project'].each do |project|
