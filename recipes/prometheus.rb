#
# Cookbook:: project
# Recipe:: prometheus
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

include_recipe 'project::set-users'

# Prometheus exporters to be deployed here below.

### DEFINE LIST OF COMPONENTS TO BE INSTALLED

install = node['prometheus']['exporters_enabled']

# use in development
#install = []
#install.push('node_exporter', 'pushgateway')
#install.push('node_exporter', 'mysqld_exporter', 'memcached_exporter')

### SETUP EXECUTABLES

install.each do |pkg|
  pkg_filename = pkg + "-" + node['prometheus'][pkg]['version'] + ".linux-amd64.tar.gz"
  install_dir = File.join(node['prometheus']['install_dir'], pkg)
  pkg_installed = File.join(node['prometheus']['install_dir'], pkg, pkg)
  remote_file File.join('/tmp', pkg_filename) do
    source node['prometheus'][pkg]['download_url']
    checksum node['prometheus'][pkg]['checksum']
    owner 'root'
    group 'root'
    mode '0755'
    not_if { ::File.exist?(pkg_installed) }
  end
  # --strip-components option strips off the first directory and extracts the rest
  bash 'install prometheus package' do
    cwd '/tmp'
    code <<-EOH
      mkdir -p #{install_dir}
      tar xzf #{pkg_filename} -C #{install_dir} --strip-components 1
      EOH
    not_if { ::File.exist?(pkg_installed) }
  end
  link File.join(node['prometheus']['install_dir'], 'bin' ,pkg) do
    to pkg_installed
    link_type :symbolic
  end
end

### SETUP CONFIGURATION FILES

# generate list of domain names and set ssl_selfsigned_status
query_domain_names = []
ssl_selfsigned_status = false
node['project']['project'].each do |project|
  query_domain_names.push(node[project]['domain'])
  query_domain_names.push("www.#{node[project]['domain']}") if node[project]['domain_www_prefix']
  if ssl_selfsigned_status == false
    ssl_selfsigned_status = !(node[project]['domain'] == node[project]['default_domain'])
  end
end

# required: templates/default/prometheus/#{cfg}.yml.erb
install.each do |cfg|
  template "#{File.join(node['prometheus']['install_dir'], 'etc')}/#{cfg}.yml" do
    source "prometheus/#{cfg}.yml.erb"
    action :create
    notifies :restart, "runit_service[#{cfg}]"
    variables({
      :config_dir => File.join(node['prometheus']['install_dir'], 'etc'),
      :environment => node.chef_environment,
      :query_names => query_domain_names,
      :ssl_enabled => node['nginx']['ssl']['enabled'],
      :ssl_selfsigned => ssl_selfsigned_status
    })
  end
end

### SETUP RUNIT

include_recipe 'runit'

# required: templates/default/sv-#{srvc}-run.erb
install.each do |srvc|
  srvc_binary = File.join(node['prometheus']['install_dir'], srvc, srvc)
  runit_service srvc do
    default_logger true
    owner node['project']['ops_user']
    group node['project']['ops_user']
    options({
      :user => node['project']['ops_user'],
      :binary => srvc_binary,
      :parameters => node['prometheus'][srvc]['parameters']
    }.merge(params))
    action [:create, :enable]
  end
end


### PROMETHEUS

include_recipe 'prometheus::default'


include_recipe "prometheus::use_lwrp"
# otherwise `service[prometheus] cannot be found in the resource collection`

# create jobs with template
template node['prometheus']['flags']['config.file'] do
  action    :create
  cookbook  node['prometheus']['job_config_cookbook_name']
  source    node['prometheus']['job_config_template_name']
  mode      '0644'
  owner     node['prometheus']['user']
  group     node['prometheus']['group']
  variables(
  )
end

runit_service 'prometheus' do
  action :restart
end

### SCRIPT EXECUTER

# creates a script that can run commands in crontasks and export the result to Prometheus

template node['prometheus']['script_executer'] do
  source    'prometheus/script_executer.erb'
  mode      '0755'
  owner     'root'
  group     'root'
  action    :create
end
