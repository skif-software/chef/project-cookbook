#
# Cookbook:: project
# Recipe:: php
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

## curl is not installed by default on many systems
package 'curl'

# To delete old php packages, enter:
# x="$(dpkg --list | grep php | awk '/^ii/{ print $2}')"
# sudo apt-get --purge remove $x

apt_repository 'dotdeb' do
  uri        'http://packages.dotdeb.org'
  components ['all']
  key 'https://www.dotdeb.org/dotdeb.gpg'
  action :add
end

include_recipe 'php'

# https://supermarket.chef.io/cookbooks/php-fpm
# https://github.com/yevgenko/cookbook-php-fpm/blob/master/templates/default/pool.conf.erb

# ENABLE STATUS PAGE
# If status page should be enabled please add the following to configs:
# echo "pm.status_path = /status" >> /etc/php5/fpm/pool.d/default.conf
# service php5-fpm reload
# vim /etc/nginx/conf.d/websites.inc
#  location /status {
#     access_log off;
#     fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
#     include fastcgi_params;
#     fastcgi_pass  unix:/var/run/php5-fpm.sock;
#  }
# nginx -t
# service nginx reload

# Install depricated mysql.so from source code
if node['php']['mysql']['install_mysql.so_extension']

  # check mysql.so already installed
  check_mysql_extension_installed = Mixlib::ShellOut.new("ls $(php-config --extension-dir)/mysql.so")
  if check_mysql_extension_installed.run_command != true

    git '/tmp/mysql.so.source' do
      repository "https://github.com/php/pecl-database-mysql"
      revision   'master'
      enable_checkout false
      enable_submodules true
      user       'root'
      group      'root'
      action     :checkout
    end

    execute 'configure mysql.so.source' do
      command %Q[phpize; ./configure]
      cwd     '/tmp/mysql.so.source'
      user       'root'
      group      'root'
      action  :run
    end

    execute 'make and install mysql.so' do
      command %Q[make && make install]
      cwd     '/tmp/mysql.so.source'
      user       'root'
      group      'root'
      action  :run
    end

    if check_mysql_extension_installed.run_command
      log 'check_mysql_extension_installed' do
        message "mysql.so extension successfully installed"
      end
    end

    #add param to fpm pool config (in project's cookbook)
    #php_admin_value[extension] = mysql.so

    bash 'cleanup mysql.so.source' do
      cwd '/tmp'
      code <<-EOH
        rm -rf /tmp/mysql.so.source
        EOH
    end

  else

    log 'check_mysql_extension_installed' do
      message "mysql.so extension already installed, passing by"
    end

  end # if check_mysql_extension_installed.run_command

end # if node['php']['mysql']['install_mysql.so_extension']

# Install depricated memcache.so from source code
if node['php']['memcached']['install_memcache.so_extension']

  # check memcache.so already installed
  check_memcache_extension_installed = Mixlib::ShellOut.new("ls $(php-config --extension-dir)/memcache.so")
  if check_memcache_extension_installed.run_command != true

    git '/tmp/memcache.so.source' do
      repository "https://github.com/websupport-sk/pecl-memcache.git"
      revision   'master'
      enable_checkout false
      enable_submodules true
      user       'root'
      group      'root'
      action     :checkout
    end

    execute 'configure memcache.so.source' do
      command %Q[phpize; ./configure]
      cwd     '/tmp/memcache.so.source'
      user       'root'
      group      'root'
      action  :run
    end

    execute 'make and install mysql.so' do
      command %Q[make && make install]
      cwd     '/tmp/memcache.so.source'
      user       'root'
      group      'root'
      action  :run
    end

    if check_memcache_extension_installed.run_command
      log 'check_memcache_extension_installed' do
        message "memcache.so extension successfully installed"
      end
    end

    #add param to fpm pool config (in project's cookbook)
    #php_admin_value[extension] = memcache.so

    bash 'cleanup memcache.so.source' do
      cwd '/tmp'
      code <<-EOH
        rm -rf /tmp/memcache.so.source
        EOH
    end

  else

    log 'check_memcache_extension_installed' do
      message "memcache.so extension already installed, passing by"
    end

  end # if check_memcache_extension_installed.run_command

end # if node['php']['memcached']['install_memcache.so_extension']

# INSTALL FPM POOL
node['project']['project'].each do |project|
  username = project
  mysqld_sock = File.join(node['mysql']['service']['socket_dir'], "mysql-#{project}", 'mysqld.sock')
  mysqli_default_socket = { 'php_value[mysqli.default_socket]' => "#{mysqld_sock}" }

  php_fpm_pool project do
    max_children node['php']['fpm_pool']['max_children']
    start_servers node['php']['fpm_pool']['start_servers']
    min_spare_servers node['php']['fpm_pool']['min_spare_servers']
    max_spare_servers node['php']['fpm_pool']['max_spare_servers']
    listen "/var/run/#{node['php']['fpm_service']}_#{project}.sock"
    user username
    group username
    additional_config(node['php']['fpm'][project].merge(mysqli_default_socket))
    action :install
  end
end

# see this issue for workarounds on config https://github.com/chef-cookbooks/php/issues/116
    #additional_config ({
    #  'mbstring.func_overload' => '2',
    #  'mbstring.internal_encoding' => 'UTF-8',
    #})
# https://docs.chef.io/dsl_recipe.html#edit-resource
#edit_resource!(:template, "#{node['php']['conf_dir']}/php.ini" ) do
#  cookbook 'MY_WRAPPER_COOKBOOK'
#  lazy { notifies :restart, "service[#{node['php']['fpm_service']}]", :delayed }
#end

service node['php']['fpm_service'] do
  action :start
end

template File.join(node['php']['fpm_conf_dir'], 'php.ini') do
  source node['php']['fpm_ini']['template']
  variables({
    :directives => node['php']['fpm_directives']
  })
  action :create
  notifies :restart, "service[#{node['php']['fpm_service']}]", :delayed
end
