#
# Cookbook:: project
# Recipe:: percona-xtrabackup
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

# https://www.percona.com/doc/percona-xtrabackup/LATEST/installation/apt_repo.html
# see note about keys https://www.percona.com/blog/2016/10/13/new-signing-key-for-percona-debian-and-ubuntu-packages/
apt_repository 'percona-release' do
  uri          'http://repo.percona.com/apt'
  distribution  node['lsb']['codename']
  components   ['main']
  key          '8507EFA5'
  keyserver    'keyserver.ubuntu.com'
  action       :add
  deb_src       true
end

apt_update 'update'

package 'percona-xtrabackup-24'
