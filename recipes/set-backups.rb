#
# Cookbook:: project
# Recipe:: set-backups
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# Configures all the backups performed by the Backup gem

include_recipe 'project::backupgem'
include_recipe 'project::percona-xtrabackup'

home = node['project']['project_home']

node['project']['project'].each do |project|

username    = project
project_home = File.join(home, project)


%w(backups backups/models backups/data).each do |dir|
  directory File.join(project_home, dir) do
    owner username
    group username
    mode '0755'
    action :create
  end
end

template File.join(project_home, 'backups', 'config.rb') do
  source 'backupgem/config.rb.erb'
  variables({
    'root_path' => File.join(project_home, 'backups'),
    'data_path' => File.join(project_home, 'backups', 'data'),
    'log_path'  => File.join(project_home, 'logs')
  })
  owner username
  group username
  mode '0755'
  action :create
end

project_database_name = node['database'][project]['name']
mysqld_sock = File.join(node['mysql']['service']['socket_dir'], "mysql-#{project}", 'mysqld.sock')

user username do
  group  "#{node['mysql']['service']['run_user']}-#{project}"
  action :modify
end

template File.join(project_home, 'backups', 'models', 'mysql.rb') do
  source 'backupgem/mysql.rb.erb'
  variables({
    'database_name' => project_database_name,
    'database_user' => node['database'][project]['admin_user'],
    'database_password' => node['database'][project]['admin_password'],
    'socket' => mysqld_sock,
    'data_path' => File.join(project_home, 'backups', 'data'),
    'keep_number' => 5
  })
  owner username
  group username
  mode '0755'
  action :create
end

template File.join(project_home, 'backups', 'readme.txt') do
  source 'backupgem/readme.txt.erb'
  owner username
  group username
  mode '0755'
  action :create
end

file "#{File.join(project_home, 'logs')}/backup.log" do
  user username
  group username
  action :create
end

end # |project|

# NOTE: backup cron tasks should be set in project's configuration cookbook!
#       this recipe only configures the system to perform backups!
