#
# Cookbook:: project
# Recipe:: acme
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# Installs and configures ACME command line tool
# ACME client gem is installed during chef-client run if no certs provided in .project configuration
# see resources/ssl_certs.rb

### CREATE ACME WEBROOT DIRECTORY

directory node['project']['acme']['nginx_root'] do
  owner node['nginx']['user']
  group node['nginx']['group']
  recursive true
  action :create
end

### INSTALL AND CONFIGURE ACME TOOL
## Install binary

if node['acmetool']['enabled']
  pkg_filename = 'acmetool' + "-" + node['acmetool']['version'] + "-linux-amd64.tar.gz"
  install_dir = File.join(node['acmetool']['install_dir'], 'acmetool', node['acmetool']['version'])
  pkg_installed = File.join(node['acmetool']['install_dir'], 'acmetool', node['acmetool']['version'], 'bin', 'acmetool')
  remote_file File.join('/tmp', pkg_filename) do
    source node['acmetool']['download_url']
    checksum node['acmetool']['checksum']
    owner 'root'
    group 'root'
    mode '0755'
    not_if { ::File.exist?(pkg_installed) }
  end
  # --strip-components option strips off the first directory and extracts the rest
  bash 'install acmetool package' do
    cwd '/tmp'
    code <<-EOH
      mkdir -p #{install_dir}
      tar xzf #{pkg_filename} -C #{install_dir} --strip-components 1
      EOH
    not_if { ::File.exist?(pkg_installed) }
  end
  link File.join(node['acmetool']['install_dir'], 'bin', 'acmetool') do
    to pkg_installed
    link_type :symbolic
  end
end # if node['acmetool']['enabled']

## Configure state directory

directory node['acmetool']['state_dir'] do
  owner 'root'
  group 'root'
  recursive true
  action :create
end

directory File.join(node['acmetool']['state_dir'], 'conf') do
  owner 'root'
  group 'root'
  recursive true
  action :create
end

template File.join(node['acmetool']['state_dir'], 'conf', 'responses') do
  source 'acmetool/responses.erb'
  owner  'root'
  group  'root'
  variables({
    :email           => node['project']['acme']['email'],
    :server          => node['project']['acme']['server'],
    :method          => node['project']['acme']['method'],
    :webroot_path    => node['project']['acme']['webroot'],
    :install_cronjob => node['project']['acme']['install_cronjob'],
    :key_type        => node['project']['acme']['key_type'],
    :rsa_key_size    => node['project']['acme']['rsa_key_size']
  })
  action :create
  notifies :run, 'execute[acmetool_quickstart_batch]', :immediately
end

# Generate state and register with ACME server
execute "acmetool_quickstart_batch" do
  command %Q[acmetool quickstart --batch]
  action :nothing
end


## Setup crontask to check and update certs

cron 'ACME' do
  action :create
  minute '0'
  hour '0'
  weekday '*'
  user 'root'
  environment ({'PROMETHEUS_TAG_NAME' => "\"acme\""})
  home '/tmp'
  command %Q[script_executer #{File.join(node['acmetool']['install_dir'], 'bin', 'acmetool')} --batch reconcile]
end

log 'message' do
  message "IMPORTANT: System cron task to update ssl certs for the projects on this server has been setup (see acme.md in documentation)."
  level :info
end
