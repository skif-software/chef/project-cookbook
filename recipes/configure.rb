#
# Cookbook:: project
# Recipe:: configure
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

node['project']['project'].each do |project|
  include_recipe "#{project}::configure"
end
