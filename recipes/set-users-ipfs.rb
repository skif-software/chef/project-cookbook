#
# Cookbook:: project
# Recipe:: set-users-ipfs
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

include_recipe 'project::ipfs'

home = node['project']['project_home']

node['project']['project'].each do |project|

  username    = project

  directory File.join(home, project, '.ipfs') do
    owner username
    group username
    recursive true
    action :create
  end

  file File.join(home, project, '.ipfs', 'api') do
    content node['ipfs']['api']
    mode '0755'
    owner username
    group username
  end

end # |project|
