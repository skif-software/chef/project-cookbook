#
# Cookbook:: project
# Recipe:: set-configuration
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

# Configuration of the core Bitrix parameters
# https://dev.1c-bitrix.ru/learning/course/?COURSE_ID=43&LESSON_ID=2795
# some useful info about Bitrix connections to MySQL
# https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=32&LESSON_ID=3354&LESSON_PATH=3903.4897.4898.3354

home = node['project']['project_home']

node['project']['project'].each do |project|

  username    = project
  server_root = File.join(home, project, node['project']['project_server_root'])
  memcached_socket = "/tmp/memcached_#{project}.sock"

  project_id = Project.new(project)

  %w{extra dbconn}.each do |s|
    template File.join(server_root, 'bitrix', ".settings_#{s}.php") do
      source "bitrix/settings_#{s}.php.erb"
      variables({
        :bind_address => "#{node['mysql']['service']['bind_address']}:#{node['mysql']['service']['port'].to_i + project_id.to_int}",
        :db_name => node['database'][project]['name'],
        :admin_user => node['database'][project]['admin_user'],
        :admin_password => node['database'][project]['admin_password'],
        :memcached_socket => memcached_socket,
        :memcache_type => node['php']['memcached']['install_memcache.so_extension'],
        :prometheus_pushgateway => "127.0.0.1:#{node['prometheus']['pushgateway']['port']}",
        :extra_settings => node[project]['bitrix_extra_settings']
      })
      owner username
      group username
      action :create
      only_if { ::File.exist?(File.join(server_root, 'bitrix', ".settings.php")) }
    end
  end
end
