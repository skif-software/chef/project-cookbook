#
# Cookbook:: project
# Recipe:: set-ssh-config
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# Creates special user for ssh passwordless access and Installs ssh keys

home = node['project']['project_home']

node['project']['project'].each do |project|

username = project

directory File.join(home, project, '.ssh') do
  owner  username
  group  username
  mode   0700
  action :create
end


## CREATE ~/.ssh/config FOR USER TO GET CODE FROM GIT REPOSITORY JUST IN CASE THERE ARE SPECIFIC SETTINGS
# https://about.gitlab.com/2016/02/18/gitlab-dot-com-now-supports-an-alternate-git-plus-ssh-port/

template File.join(home, project, '.ssh', 'config') do
  source node[project]['ssh-keys']['ssh_config']
  variables options: {
    :host => node[project]['git']['host'],
    :port => node[project]['git']['ssh_port']
  }
  owner  username
  group  username
  action :create
end

ssh_known_hosts_entry node[project]['git']['host'] do
  port node[project]['git']['ssh_port']
end

end # |project|
