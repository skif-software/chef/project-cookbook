#
# Cookbook:: project
# Recipe:: set-sphinx-config
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# Generate Sphinx configuration for the project

node['project']['project'].each do |project|

  username    = project
  home        = node['project']['project_home']
  server_root = File.join(home, project, node['project']['project_server_root'])
  if node[project]['sphinx']['enabled']
    install_sphinx = true
  else
    install_sphinx = false
  end rescue NoMethodError

  if install_sphinx

    sphinx_home = File.join(home, project, node['project']['sphinx']['home'])
    sphinx_data = File.join(home, project, node['project']['sphinx']['data'])
    sphinx_dicts = File.join(home, project, node['project']['sphinx']['dicts'])
    sphinx_logs = File.join(home, project, node['project']['sphinx']['logs'])
    sphinx_run = File.join(home, project, node['project']['sphinx']['run'])
    sphinx_indexes = File.join(home, project, node['project']['sphinx']['indexes'])

    sphinx_config = File.join(home, project, node['project']['sphinx']['config'])
    sphinx_conf_d = File.join(home, project, node['project']['sphinx']['conf.d'])

    sphinx_scripts  = File.join(home, project, node['project']['sphinx']['scripts'])
    sphinx_autoconfig  = File.join(home, project, node['project']['sphinx']['autoconfig'])

    project_id = Project.new(project)

    sphinx_searchd_port = 9300 + project_id.to_int
    sphinx_searchd_mysqlport = 9300 + project_id.to_int + 1

    sphinx_searchd_host = node['project']['sphinx']['host']


    ## CREATE STANDARD PROJECT CONFIGURATION
    # create config directories
    [sphinx_home, sphinx_conf_d, sphinx_data, sphinx_dicts, sphinx_logs, sphinx_run, sphinx_indexes, sphinx_scripts, sphinx_autoconfig].each do |dir|
    directory dir do
      owner username
      group username
      mode '0750'
      action :create
    end
    end # |dir|

    # install dictionaries
    %w{de.pak en.pak ru.pak}.each do |dic|
    cookbook_file File.join(sphinx_dicts, dic) do
      source "sphinx/dicts/#{dic}"
      owner username
      group username
      mode '0750'
      action :create
    end
    end # |dic|

    # create main configuration script
    template sphinx_config do
      source "sphinx/sphinx.conf.erb"
      variables({
        'sphinx_inx_dir'           => sphinx_indexes,
        'sphinx_conf_dir'          => sphinx_conf_d,
        'sphinx_auto_dir'          => sphinx_autoconfig
      })
      owner username
      group username
      mode '0750'
      action :create
    end

    # create configuration files
    %w{searchd.conf indexer.conf common.conf}.each do |config|
    template File.join(sphinx_conf_d, config) do
      source "sphinx/#{config}.erb"
      variables({
        'sphinx_inx_dir'           => sphinx_indexes,
        'sphinx_general_listen'    => "#{sphinx_searchd_host}:#{sphinx_searchd_port}",
        'sphinx_mysqlproto_listen' => "#{sphinx_searchd_host}:#{sphinx_searchd_mysqlport}",
        'sphinx_main_log'          => File.join(sphinx_logs, 'searchd.log'),
        'sphinx_query_log'         => File.join(sphinx_logs, 'query.log'),
        'sphinx_run_file'          => File.join(sphinx_run, 'searchd.pid'),
        'sphinx_lib_dir'           => sphinx_data,
        'sphinx_dicts_dir'         => sphinx_dicts
      })
      owner username
      group username
      mode '0750'
      action :create
    end
    end # |config|

    # set default index
    if node['project']['sphinx']['default']
      sphinx_default_index = 'default'

      # create default index configuration file
      template File.join(sphinx_indexes, "#{sphinx_default_index}.conf") do
        source "sphinx/#{sphinx_default_index}.conf.erb"
        variables({
          'sphinx_lib_dir'           => sphinx_data
        })
        owner username
        group username
        mode '0750'
        action :create
      end
    else
      sphinx_default_index = node[project]['sphinx']['index']
    end

    # create config directories
    [File.join(sphinx_data, sphinx_default_index), File.join(sphinx_scripts, sphinx_default_index)].each do |dir|
    directory dir do
      owner username
      group username
      mode '0750'
      action :create
    end
    end # |dir|

    # create php scripts
    %w{create_index.php delete_index.php run_index.php}.each do |script|
      template File.join(sphinx_scripts, sphinx_default_index, script) do
        source "sphinx/#{script}.erb"
        variables({
          'server_root' => server_root,
          'project' => project,
          'sphinx_mysqlproto_listen' => "#{sphinx_searchd_host}:#{sphinx_searchd_mysqlport}",
          'sphinx_index_name' => sphinx_default_index
        })
        user   username
        group  username
        mode   0640
        action :create
      end
    end # |script|

    ## INCLUDE SPECIFIC PROJECT CONFIGURATION PROCEDURES
    begin
      include_recipe "#{project}::sphinx"
      rescue Chef::Exceptions::RecipeNotFound
      log 'message' do
        message "There is no specific actions on Sphinx in the project: #{project}. Passing by."
        level :info
      end
    end

  end # if install_sphinx

end # |project|
