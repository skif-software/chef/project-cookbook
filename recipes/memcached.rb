#
# Cookbook:: project
# Recipe:: memcached
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

#include_recipe 'memcached'

node['project']['project'].each do |project|
  username    = project
  memcached_instance project do
    memory node['memcached']['memory']
    user username
    extra_cli_options ["-a 0766", "-s /tmp/memcached_#{project}.sock"]
    action :start
  end
end
