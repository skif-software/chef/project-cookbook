#
# Cookbook:: project
# Recipe:: set-users
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

home = node['project']['project_home']

node['project']['project'].each do |project|

username    = project
server_root = File.join(home, project, node['project']['project_server_root'])

  user project do
    home File.join(home, project)
    shell '/bin/bash'
    action :create
  end

  directory File.join(home, project) do
    owner username
    group username
    mode '0755'
    action :create
  end

  directory File.join(home, project, 'logs') do
    owner username
    group username
    mode '0755'
    action :create
  end

  directory server_root do
    owner username
    group username
    mode '0755'
    action :create
  end
end

user node['project']['ops_user'] do
  home node['project']['ops_home']
  shell '/bin/bash'
  action :create
end

directory node['project']['ops_home'] do
  owner node['project']['ops_user']
  group node['project']['ops_user']
  mode '0750'
  action :create
end
