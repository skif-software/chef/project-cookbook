#
# Cookbook:: project
# Recipe:: msmtp
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

include_recipe 'msmtp::no_postfix'
include_recipe 'msmtp::no_exim4'
include_recipe 'msmtp::default'
