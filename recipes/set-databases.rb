#
# Cookbook:: project
# Recipe:: set-databases
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# This recipe creates users and databases, restores databases from dumps.

### PREREQUISITES

include_recipe "project::mysql"

# Install these for building mysql2 Ruby gem:
package 'libmysqlclient-dev'
include_recipe "build-essential"

# Install the mysql2 Ruby gem.
# BUG: site-cookbooks/mysql2_chef_gem/resources/mysql2_chef_gem_mysql.rb
# added package_name list to install because we use mysql apt repo
fix_mysql2_chef_gem 'default' do
  package_version '5.7'
  package ['mysql-client', 'libmysqlclient-dev']
  action :install
end


### CREATE USERS AND DATABASES

## CREATE USERS AND DATABASES FOR EACH WEBSITE ON WEBNODE
node['project']['project'].each do |project|
  project_database_name = node['database'][project]['name']
  mysqld_sock = File.join(node['mysql']['service']['socket_dir'], "mysql-#{project}", 'mysqld.sock')
  project_database 'create database' do
    db_name project_database_name
    db_admin node['database'][project]['admin_user']
    db_admin_password node['database'][project]['admin_password']
    db_user node['database'][project]['user']
    db_user_password node['database'][project]['user_password']
    mysqld_sock mysqld_sock
    action :create
  end
end

### CHECK IF DATABASES ARE EMPTY

#   This is a very important action while on production.
#   We should make it as much idempotent as possible.
#   Make sure the databases we are going to restore are empty!
#   That is why we need to run several tests.

### RESTORE DATABASES FROM DUMP

home = node['project']['project_home']

node['project']['project'].each do |project|
  username    = project
  server_root = File.join(home, project, node['project']['project_server_root'])

  config = node['database'][project]
  database = config['name']
  database_file = File.join(server_root, '.project/database.sql.gz')
  admin_user = config['admin_user']
  admin_password = config['admin_password']
  mysqld_sock = File.join(node['mysql']['service']['socket_dir'], "mysql-#{project}", 'mysqld.sock')
  execute "restore database #{database}" do
    command %Q[gunzip -c #{database_file} | mysql -S #{mysqld_sock} -u #{admin_user} -p#{admin_password} #{database}]
    user username
    action :run
    not_if %Q[mysql -S #{mysqld_sock} -u #{admin_user} -p#{admin_password} #{database} -e 'SHOW TABLES' | grep 'Tables']
    only_if { ::File.exist?(database_file) }
  end
end
