#
# Cookbook:: project
# Recipe:: set-sphinx-bitrix
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# Create default index configuration in Bitrix.

node['project']['project'].each do |project|

  username    = project
  home        = node['project']['project_home']
  if node[project]['sphinx']['enabled']
    install_sphinx = true
  else
    install_sphinx = false
  end rescue NoMethodError

if install_sphinx

  if node['project']['sphinx']['default']
    sphinx_default_index = 'default'
  else
    sphinx_default_index = node[project]['sphinx']['index']
  end

  sphinx_home = File.join(home, project, node['project']['sphinx']['home'])
  sphinx_scripts  = File.join(home, project, node['project']['sphinx']['scripts'])

  # run php script
  execute "create default index configuration in Bitrix" do
    command %Q[php -f "#{File.join(sphinx_scripts, sphinx_default_index, 'create_index.php')}"]
    cwd    File.join(home, project)
    user   username
    group  username
    action :run
  end

  end # if install_sphinx
end # |project|
