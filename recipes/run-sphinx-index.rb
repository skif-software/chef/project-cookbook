#
# Cookbook:: project
# Recipe:: run-sphinx-index
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# Run reindex operation in Bitrix

node['project']['project'].each do |project|

  username    = project
  home        = node['project']['project_home']
  server_root = File.join(home, project, node['project']['project_server_root'])

  sphinx_home = File.join(home, project, node['project']['sphinx']['home'])
  sphinx_scripts  = File.join(home, project, node['project']['sphinx']['scripts'])
  sphinx_scripts_default  = File.join(home, project, node['project']['sphinx']['scripts'], 'default')

  # run php script to start reindex operation
  execute "start reindex operation in Bitrix" do
    command %Q[php -f "#{File.join(sphinx_scripts_default, 'run_index.php')}"]
    cwd    File.join(home, project)
    user   username
    group  username
    action :run
  end

end # |project|
