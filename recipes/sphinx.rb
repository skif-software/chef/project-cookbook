#
# Cookbook:: project
# Recipe:: sphinx
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

# CHECK SPHINX IS ENABLED

install_sphinx = []
node['project']['project'].each do |project|
  if node[project]['sphinx']['enabled']
    install_sphinx.push(true)
    node[project]['sphinx']['dep_packages'].each do |p|
      package p
    end
  else
    install_sphinx.push(false)
  end rescue NoMethodError
end # |project|

if install_sphinx.include?(true)

  # INSTALL SPHINX
  include_recipe 'sphinx'

  # CREATE CONFIGURATION FOR ALL THE PROJECTS
  include_recipe 'project::set-sphinx-config'

  # CREATE SEARCHD SERVICE FOR EVERY PROJECT
  include_recipe 'project::set-sphinx-service'

  # CREATE SPHINX INDEX IN BITRIX
  include_recipe 'project::set-sphinx-bitrix'

  # START SPHINX REINDEX IN BITRIX
  # can be used in project's sphinx recipe
  #include_recipe 'project::run-sphinx-index'

end # if install_sphinx.include?(true)
