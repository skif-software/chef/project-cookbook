#
# Cookbook:: project
# Recipe:: default
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

# PREPARE SERVER
include_recipe 'project::set-users'
include_recipe 'project::set-ssh-keys'
include_recipe 'project::set-ssh-config'
include_recipe 'project::set-admin-utils'

# GET CODE AND FILES
include_recipe 'project::get-code'
include_recipe 'project::get-files'

# CREATE CONFIGURATION FOR MYSQL CLIENT
include_recipe 'project::set-users-mysql'

# INSTALL, CREATE AND CONFIGURE DATABASES
include_recipe 'project::set-databases'

# CREATE PROJECT CONFIGURATION FILES
include_recipe 'project::set-configuration'

# CREATE PROJECT MAIL CONFIGURATION
include_recipe 'project::set-users-mail'

# INSTALL MAIN SOFTWARE
include_recipe 'project::php'
include_recipe 'project::acme' if node['nginx']['acme']['enabled']
include_recipe 'project::nginx'

# INSTALL ADDITIONAL SOFTWARE
include_recipe 'project::memcached'
include_recipe 'project::sphinx'

# RUN SPECIFIC CONFIGURATION PROCEDURES
include_recipe 'project::configure'

# INSTALL BACKUP SOFTWARE AND CONFIGURE BACKUPS
include_recipe 'project::set-backups'
