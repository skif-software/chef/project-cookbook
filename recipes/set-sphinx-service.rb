#
# Cookbook:: project
# Recipe:: set-sphinx-service
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# Create Sphinx searchd service for every project.
# 
# Runit service is restarted every time this recipe is run
#        to catch possible changes in SPHINX configuration.

home = node['project']['project_home']

node['project']['project'].each do |project|

username = project
sphinx_home = File.join(home, project, 'sphinx')
if node[project]['sphinx']['enabled']
  install_sphinx = true
else
  install_sphinx = false
end rescue NoMethodError

if install_sphinx
  runit_service "searchd_#{project}" do
    run_template_name 'searchd'
    default_logger true
    owner username
    group username
    supervisor_owner username
    supervisor_group username
    options({
      :user => username,
      :binary => File.join(node['sphinx']['binary_path'], 'searchd'),
      :config => File.join(sphinx_home, 'sphinx.conf')
    }.merge(params))
    action [:create, :enable, :restart]
  end
end # if install_sphinx

end # |project|
