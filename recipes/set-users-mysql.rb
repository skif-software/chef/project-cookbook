#
# Cookbook:: project
# Recipe:: set-users-mysql
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

# Documentation links:
# Running Multiple MySQL Instances on One Machine
#   https://dev.mysql.com/doc/refman/5.7/en/multiple-servers.html
# Using Client Programs in a Multiple-Server Environment
#   https://dev.mysql.com/doc/refman/5.7/en/multiple-server-clients.html
# MySQL Program Environment Variables
#   https://dev.mysql.com/doc/refman/5.7/en/environment-variables.html
# Can't connect to [local] MySQL server
#   https://dev.mysql.com/doc/refman/5.7/en/can-not-connect-to-server.html

home = node['project']['project_home']

node['project']['project'].each do |project|

  username    = project

  file File.join(home, project, '.my.cnf') do
    content %Q[[client]
socket = #{File.join(node['mysql']['service']['socket_dir'], "mysql-#{project}", 'mysqld.sock')}]
    mode '0755'
    owner username
    group username
  end

end # |project|
