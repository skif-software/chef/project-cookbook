#
# Cookbook:: project
# Recipe:: get-ssl-certs
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# Generates the certs with ACME tool client

include_recipe 'project::acme'

node['project']['project'].each do |project|

username    = project

domain = node[project]['domain']

execute "acmetool_want" do
  command     %Q[acmetool want --batch #{domain} www.#{domain} ]
  action      :run
  notifies    :create, 'link[live_ssl]', :immediately
  notifies    :reload, 'service[nginx]', :delayed
end

link 'live_ssl' do
  target_file File.join(node['nginx']['ssl']['live'], domain)
  to          File.join(node['project']['acme']['live'], domain)
  action      :nothing
  notifies    :delete, 'directory[tmp_cert_ssl]', :delayed
end

directory 'tmp_cert_ssl' do
  path        File.join(node['nginx']['ssl']['certs'], domain)
  recursive   true
  action      :nothing
end

service 'nginx' do
  action      :nothing
end

end # |project|
