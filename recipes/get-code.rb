#
# Cookbook:: project
# Recipe:: get-code
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

package 'git'

include_recipe 'project::set-users'
include_recipe 'project::set-ssh-keys'
include_recipe 'project::set-ssh-config'

home = node['project']['project_home']

node['project']['project'].each do |project|

username    = project
server_root = File.join(home, project, node['project']['project_server_root'])

git server_root do
  repository "git@#{node[project]['git']['host']}:#{node[project]['git']['repo']}"
  revision node[project]['git']['branch'] || node['project']['git']['branch']
  enable_checkout false
  user username
  group username
  action :checkout
end

execute 'secure .project directory' do
  command %Q[chmod -R 0640 #{File.join(server_root, '.project')}]
  cwd    server_root
  user   username
  group  username
  action :run
  only_if { File.exist?(File.join(server_root, '.project')) }
end

end # |project|
