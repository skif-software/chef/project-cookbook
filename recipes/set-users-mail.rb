#
# Cookbook:: project
# Recipe:: set-users-mail
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

include_recipe 'project::set-users'

include_recipe 'project::msmtp'

home = node['project']['project_home']

node['project']['project'].each do |project|

username  = project

if node[project]['mail']
account   = { project => node[project]['mail'] }

msmtp_account 'default' do
  user username
  config account
end
end # if

end # |project|
