#
# Cookbook:: project
# Recipe:: backupgem
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# Installs backup gem

# Support for Ruby 2.4.0+ will be released in Backup version 5
package 'build-essential'
package 'ruby'
package 'ruby-dev'

gem_package 'backup' do
  version '4.4.0'
  action :install
end
