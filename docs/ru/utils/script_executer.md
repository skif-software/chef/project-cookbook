# Script executer

Скрипт устанавливается на сервере в `/usr/bin/script_executer` в рамках автоматизированной процедуры `prometheus.rb`.

Скрипт позволяет исполнять любые команды и экспортировать их результат в системную метрику, например:  
`export PROMETHEUS_TAG_NAME="start_bitrix_cron_events" && \`  
`script_executer php /home/project/www/bitrix/modules/main/tools/cron_events.php`  
экспортирует метрику  
`web_cron_task_success{name="start_bitrix_cron_events"} 1`

Для работы скрипта необходимо установить переменные окружения:  
* _PROMETHEUS_METRIC_NAME_
* _PROMETHEUS_TAG_NAME_

Если не установлена переменная _PROMETHEUS_METRIC_NAME_, то используется по умполчанию значение _web_cron_task_success_. Если не установлена переменная _PROMETHEUS_TAG_NAME_, то скрипт заканчивается ошибкой.

Скрипт можно использовать в задачах Cron.
