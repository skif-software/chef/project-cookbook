# SPHINX

## Программная конфигурация подсистемы полнотекстового поиска SPHINX

Автоматизированные процедуры `recipes/`:
* sphinx.rb             - общая процедура;
* set-sphinx-config.rb  - процедура создания конфигурации `sphinx` и интерфейса управления информационными процессами поисковой подсистемы;
* set-sphinx-service.rb - процедура создания системной службы `searchd`;
* set-sphinx-bitrix.rb  - процедура информационного процесса по регистрации поискового индекса в Bitrix;
* run-sphinx-index.rb   - процедура информационного процесса по созданию поискового индекса в Bitrix.

Шаблоны `templates/default/sphinx/`, `templates/default/`:
* sphinx.conf.erb
* common.conf.erb
* searchd.conf.erb
* indexer.conf.erb
* default.conf.erb
* create_index.php.erb
* delete_index.php.erb
* run_index.php.erb
* sv-searchd-run.erb

Объекты `resources/`:
* searchd.rb - системная служба `searchd`.

Атрибуты `attributes/`:
* sphinx.rb

## Использование

### Базовая программная конфигурация подсистемы
Для использования поисковой подсистемы SPHINX в программной конфигурации информационной системы необходимо установить атрибут `default[project_id]['sphinx']['enabled'] = true`.

### Дополнительная программная конфигурация подсистемы

В случае, если необходимо произвести дополнительные действия по настройке подсистемы полнотекстового поиска в комплекте автоматизированных процедур программной конфигурации информационной системы нужно создать процедуру `sphinx.rb`. Данная процедура будет выполнена в конце процедуры конфигурации `set-sphinx-config.rb` комплекта автоматизированных процедур `project-cookbook`.

Возможные дополнительные действия по настройке подсистемы полнотекстового поиска описаны ниже.

#### Отключить типовой индекс
В программной конфигурации информационной системы необходимо установить атрибуты `default['project']['sphinx']['default'] = false` и `default[project_id]['sphinx']['index'] = index_name`, где _index_name_ - название индекса. При этом конфигурационный файл индекса `File.join(node['project']['project_home'], node[project_id]['project_id'], node['project']['sphinx']['indexes'], 'index_name.conf')` необходимо создать в рамках процедуры `sphinx.rb` программной конфигурации информационной системы.

#### Создать индекс в project
В программной конфигурации информационной системы можно использовать процедуру `include_recipe 'project::run-sphinx-index'` для создания индекса в Bitrix.

#### Включить в конфигурационный файл `sphinx.rb` дополнительные инструкции
Для выполнения каких-либо действий перед запуском `searchd` либо `indexer` в директории `File.join(node['project']['project_home'], node[project_id]['project_id'], node['project']['sphinx']['autoconfig'])` необходимо создать символьную ссылку на скриптовый файл, который можно создать в директории `File.join(node['project']['project_home'], node[project_id]['project_id'], node['project']['sphinx']['scripts'], 'index_name')`.

#### Перезапустить системную службу
В программной конфигурации информационной системы можно использовать процедуру `include_recipe "project::set-sphinx-service"` либо объект project_searchd:  
```
project_searchd 'restart searchd' do
  project node[project_id]['project_id']
  action :restart
end
```
