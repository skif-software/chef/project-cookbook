TODO:
* Edit comments.
* Add documentation.
* Edit CONTRIBUTING.md
* Add support for ubuntu os.
* Add support for bitrix type of projects.
* Edit dependencies in metadata.rb and Berksfile (?publish modified cookbooks).
* Release public (v1.0.0).

0.6.2

* Fix permissions conflict with git.

0.6.1

* Fix database resource and user mysql config.

0.6.0

* Add support for multiple instances of MySQL.
* Implement Backup gem.
* Add useful utils for system administration.
* Fix ipfs permissions.
* Update ipfs version.
* Add ipfs data skip to bypass download of large binaries.
* Add project sphinx dep_packages installation.

0.5.1

* Fix install Git package.
* Fix buggy ssh_known_hosts cookbook.
* Fix failure when setting up IPFS first time.

0.5.0

* Rename cookbook.
* Add license/author information.

0.4.4

* Add support for depricated memcache.so PHP extension.

0.4.3

* Fix memcached to start after chef-client run.

0.4.2

* Fix compile and converge problem during setup of ssl_certs from `.project/ssl`.

0.4.1

* Add support for depricated mysql.so PHP extension.

0.4.0

* Implement ACME service.

0.3.4

* Implement CRON EVENTS MONITORING with Prometheus.

0.3.3

* Create NGINX fastcgi attributes.

0.3.2

* Fix SPHINX runit supervisor owner.

0.3.1

* Fix SPHINX search engine recipes.

0.3.0

* Add SPHINX search engine recipes.

0.2.9

* Add PROMETHEUS monitor labels.

0.2.8

* Set nice value 19 for IPFS daemon.

0.2.7

* Add PROMETHEUS blackbox_exporter service and dns checks.

0.2.6

* Add NETDATA mysql plugin configuration.

0.2.5

* Add metrics to PROMETHEUS from NETDATA.

0.2.4

* Add NETDATA configuration for health alert notifications.

0.2.3

* Add status metrics.

0.2.2

* Add NETDATA monitoring (standard configuration).

0.2.1

* Add Prometheus pushgateway.

0.2.0

* WIP on Prometheus.

0.1.7

* Add Prometheus monitoring (node_exporter).

0.1.6

* Fix IPFS private and update binary version.

0.1.5

* Fix mysql client default config.

0.1.4

* Fix runtime ipfs.

0.1.3

* Fix IPFS high memory usage by creating crontask to restart every hour and permisions to control service.
