property :db_name, String
property :db_admin, String
property :db_admin_password, String
property :db_user, [ String, nil ], default: nil
property :db_user_password, [ String, nil ], default: nil
property :db_query, [ String, nil ], default: nil
property :mysqld_sock, String

action :create do

     sql_server_connection_info = {
         :username => 'root',
         :socket   => mysqld_sock,
         :password => node['mysql']['service']['initial_root_password']
     }

     mysql_database db_name do
       connection sql_server_connection_info
       action :create
     end

     mysql_database_user db_admin do
       connection sql_server_connection_info
       password   db_admin_password
       provider   Chef::Provider::Database::MysqlUser
       database_name db_name
       action     [:create, :grant]
     end

     # using mysql_database_user fails because it uses db_name
     # mysql command should look like:
     # GRANT FILE ON *.* TO 'admin'@'localhost';
     # to check the GRANTs execute from mysql:
     # select user,File_priv from mysql.user;
     # note: :process, :reload, :super privs is for running percona-extrabackups
     database_user db_admin do
       connection sql_server_connection_info
       provider   Chef::Provider::Database::MysqlUser
       password   db_admin_password
       privileges [:file, :process, :reload, :super]
       action     [:grant]
     end

     if db_user
       mysql_database_user db_user do
         connection sql_server_connection_info
         password   db_user_password
         provider   Chef::Provider::Database::MysqlUser
         database_name db_name
         action     [:create, :grant]
       end
       database_user db_user do
         connection sql_server_connection_info
         provider   Chef::Provider::Database::MysqlUser
         password   db_user_password
         privileges [:select]
         action     [:grant]
       end
     end

end

action :drop do

   sql_server_connection_info = {
       :host     => node['mysql']['service']['bind_address'],
       :username => 'root',
       :socket   => mysqld_sock,
       :password => node['mysql']['service']['initial_root_password']
   }

    mysql_database db_name do
       connection sql_server_connection_info
       action :drop
    end

end

action :query do

   sql_server_connection_info = {
       :host     => node['mysql']['service']['bind_address'],
       :username => db_admin,
       :socket   => mysqld_sock,
       :password => db_admin_password
   }

    mysql_database db_name do
       connection sql_server_connection_info
       database_name db_name
       sql db_query
       action :query
    end

end
