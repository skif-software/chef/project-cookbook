# This resource sets up project's ssl cert from .project configuration or project cookbook.
# If ACME is enabled then selfigned cert is placed to fix NGINX bootstrap problem (it is replaced with production cert after NGINX is configured).

property :project, String
property :domain, String
property :server_root, String

action :setup do


  # Check if ACME live ssl certs are already in place (TODO: implement check for CN and expiry date)
  if node['nginx']['acme']['enabled'] &&
     ::File.exist?(::File.join(node['project']['acme']['live'], domain, "fullchain")) &&
     ::File.exist?(::File.join(node['project']['acme']['live'], domain, "privkey"))

     log 'message' do
       message "IMPORTANT: CHEF has found live ssl certs managed by ACME."
       level :info
     end

     link ::File.join(node['nginx']['ssl']['live'], domain) do
       to ::File.join(node['project']['acme']['live'], domain)
     end

  else

  directory ::File.join(node['nginx']['ssl']['certs'], domain) do
    owner  'root'
    group  'root'
    mode   0755
    action :create
  end

  link ::File.join(node['nginx']['ssl']['live'], domain) do
    to ::File.join(node['nginx']['ssl']['certs'], domain)
  end

  if ::File.exist?(::File.join(server_root, node['project']['project_config_dir'], 'ssl', domain, "fullchain")) &&
     ::File.exist?(::File.join(server_root, node['project']['project_config_dir'], 'ssl', domain, "privkey"))

    remote_file ::File.join(node['nginx']['ssl']['certs'], domain, "fullchain") do
      source      "file://#{::File.join(server_root, node['project']['project_config_dir'], 'ssl', domain, "fullchain")}"
      owner       'root'
      group       'root'
      mode        '0755'
      action      :create
    end
    remote_file ::File.join(node['nginx']['ssl']['certs'], domain, "privkey") do
      source      "file://#{::File.join(server_root, node['project']['project_config_dir'], 'ssl', domain, "privkey")}"
      owner       'root'
      group       'root'
      mode        '0755'
      action      :create
    end

  elsif run_context.has_cookbook_file_in_cookbook?(project, "ssl/#{domain}/fullchain") &&
        run_context.has_cookbook_file_in_cookbook?(project, "ssl/#{domain}/privkey")

    cookbook_file ::File.join(node['nginx']['ssl']['certs'], domain, "fullchain") do
      source      "ssl/#{domain}/fullchain"
      cookbook    project
      owner       'root'
      group       'root'
      mode        '0755'
      action      :create
    end
    cookbook_file ::File.join(node['nginx']['ssl']['certs'], domain, "privkey") do
      source      "ssl/#{domain}/privkey"
      cookbook    project
      owner       'root'
      group       'root'
      mode        '0755'
      action      :create
    end

  else

    log 'message' do
      message "There is no ssl certs in #{project} cookbook or in .project configuration. Chef will generate self signed certs."
      level :info
    end
    # if no ssl certs are provided in .project/ssl directory and in project's cookbook we generate selfsigned
    # to solve NGINX bootstrap problem

    # Generates the selfsigned certs with ACME client cookbook
    # Include the recipe to install the gem
    include_recipe 'acme'

    acme_selfsigned domain do
      crt   ::File.join(node['nginx']['ssl']['certs'], domain, "fullchain")
      key   ::File.join(node['nginx']['ssl']['certs'], domain, "privkey")
    end

  end #  if ::File.exist?(::File.join(server_root, node['project']['project_config_dir'], 'ssl', domain, "fullchain")) &&
      #     ::File.exist?(::File.join(server_root, node['project']['project_config_dir'], 'ssl', domain, "privkey"))

  end # if node['nginx']['acme']['enabled'] &&
      # ::File.exist?(::File.join(node['project']['acme']['live'], domain, "fullchain")) &&
      # ::File.exist?(::File.join(node['project']['acme']['live'], domain, "privkey"))
end
