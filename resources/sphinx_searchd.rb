property :project, String

action :restart do
  runit_service "searchd_#{project}" do
    action [:restart]
  end
end
