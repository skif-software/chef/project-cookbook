property :ipfs_data_file, String
property :ipfs_data_protected, Array
property :ipfs_data_skip, Array
property :server_root, String
property :username, String
property :project, String

action :get do

  if ::File.exist?(ipfs_data_file)
    ::File.open(ipfs_data_file).each do |line|
      ipfs_object = line.split(' ')
      ipfs_object_path = ::File.join(server_root, ipfs_object[0])
      execute "get the data (#{ipfs_object[0]}) from IPFS for #{project}" do
        command %Q[ipfs get --api='/ip4/127.0.0.1/tcp/5001' -o #{ipfs_object[0]} #{ipfs_object[1]}]
        cwd ::File.dirname(ipfs_data_file)
        user username
        group username
        action :run
        not_if { ipfs_data_protected.include?(ipfs_object[0]) && ::File.exist?(ipfs_object_path) }
        not_if { ipfs_data_skip.include?(ipfs_object[0]) }
      end
    end
  else
    log 'message' do
      message "place .ipfs file into #{project} root to enable IPFS ... skipping"
      level :info
    end
    return
  end

end
